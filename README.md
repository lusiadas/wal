# wal
> A plugin for [Oh My Fish](https://www.github.com/oh-my-fish/oh-my-fish).

[![GPL License](https://img.shields.io/badge/license-GPL-blue.svg?longCache=true&style=flat-square)](/LICENSE)
[![Fish Shell Version](https://img.shields.io/badge/fish-v3.0.1-blue.svg?style=flat-square)](https://fishshell.com)
[![Oh My Fish Framework](https://img.shields.io/badge/Oh%20My%20Fish-Framework-blue.svg?style=flat-square)](https://www.github.com/oh-my-fish/oh-my-fish)

<br/>


## Description

List photo collections on unsplash from where to get a wallpaper at random.

## Options

```
wal [collection number]
Get a random image from a collection and set it as the new background and screensaver wallpaper. If no collection is specified, a random listed collection will be used instead.

wal -a/--add [collection number] ...
Add a new collection to the list.

wal -r/--remove [collection number] ...
Remove a collection from the list.

wal -u/--url [collection number] ...
Present the urls from specified collections.

wal -f/-folder [directory]
Set where images are stored. If no directory is passed, display where they're currently being stored.

wal -c/--cache [number]
Set the size of the image cache.

wal -l/--list [collection/size/description]
Show listed collections.

wal -h/--help
Display these instructions.
```

## Install

```fish
$ omf install wal
```

## Configuration

This script is optimized to work with environment variables available to [cron](https://en.wikipedia.org/wiki/Cron), so that you can change your background and lockscreen wallpapers periodically and automatically. As an example, to change them every 3 hours, use the command `crontab -e` and append the following line:

```
0 */3 * * * fish -c wal
```

For more information on how to schedule tasks using cron, consult this [video](https://www.youtube.com/watch?v=8j0SWYNglcw).

## Dependencies

This plugin uses [unsplash_wallpaper](https://github.com/cuth/unsplash-wallpaper) by [cuth](https://github.com/cuth) to draw wallpapers. If you don't have it installed, you'll be prompted to install it upon the installation of this plugin.

---

Ⓐ Made in Anarchy. No wage slaves were economically coerced into the making of this work.
