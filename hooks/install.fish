function install_"$package" -V path -V package
  if test (fish --version | string match -ar '\d' | string join '') -lt 300
    echo 'This plugin is compatible with fish version 3.0.0 or above, please update before retrying to install it' 1>&2
    return 1
  end
  command wget -qO $path/dependency.fish \
  https://gitlab.com/lusiadas/dependency/raw/master/dependency.fish
  source $path/dependency.fish
  or return 1
  dependency -un $cmd -N unsplash-wallpaper grep sed curl \
  -P https://gitlab.com/lusiadas/contains_opts
end
install_"$package"
set -l exit_status $status
functions -e install_"$package" dependency
test $exit_status -eq 0
