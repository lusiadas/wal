set -l cmd (command basename (status -f) | cut -d '.' -f 1)
function $cmd -V cmd -d "List photo collections from where to get a wallpaper at random"

  # Load dependencies
  source (command dirname (status -f))/../instructions.fish
  source (command dirname (status -f))/../dependency.fish

  # Set variables
  set -l collections_list (command dirname (status -f))/../collections_list
  set --query wallpapers_folder
  or set -l wallpapers_folder $HOME/Pictures/wallpapers
  set --query wallpaper_cache
  or set -l wallpaper_cache 10

  # Load main function
  function "$cmd"_main -V cmd -V collections_list -V wallpapers_folder -V wallpaper_cache
    dependency -n $cmd -N unsplash-wallpaper grep sed curl
    or return 1
    if argparse -n $cmd -x a,r,u,f,c,l,h 'a/add' 'r/remove' 'u/url' 'f/folder=' 'c/cache=' 'l/list' 'h/help' -- $argv 2>&1 | read err
      err $err
      reg "Use |$cmd -h| to see examples of valid syntaxes"
      return 1
    end

    # Check for flags, arguments, and collection lists
    set -l flag (set --name | string match -r '(?<=^_flag_).{2,}')
    if string match -qr '(add|remove|url)' $flag
      if test -z "$argv"
        err "$cmd: Missing argument"
        "$cmd"_instructions "$cmd -$flag/--\S+"
        return 1
      end
    end
    if string match -qr '^(remove|url|list|)$' "$flag"
      if not test -s "$collections_list"
        err "$cmd: No collections list currently exists"
        reg "Start one using |$cmd --add|"
        return 1
      end
    end

    switch "$flag"
      case help
        "$cmd"_instructions
        test -z "$argv"

      case list
        command cat $collections_list
        test -z "$argv"

      case url
        printf '%s\n' https://unsplash.com/collections/{(string join , $argv)}

      case folder
        if test -z "$argv"
          echo "$wallpapers_folder"
          return 0
        end
        string match "$wallpapers_folder" "$argv"
        and return 0
        if mkdir -p "$argv" 2>&1 | read err
          err $cmd: (string match -r '(?<=mkdir: ).+'$err)
          return 1
        end
        mv $wallpapers_folder/* "$argv" 2>/dev/null
        set -U wallpapers_folder "$argv"
        win "Wallpapers folder set to |$wallpapers_folder|"

      case cache
        if string match -qvr '\d' $_flag_cache
          err "$cmd: $_flag_cache: Invalid value"
          "$cmd"_instructions "$cmd -a/--\S+"
        end
        set -U wallpaper_cache $_flag_cache
        win "Wallpaper cache size se to |$wallpaper_cache|"

      case remove
        set -l list_length (cat $collections_list | wc -l)
        for collection in $argv
          sed -ie "2,\${ /^$collection\b/d;/\b$collection\$/d }" $collections_list
        end
        test (cat $collections_list | wc -l) -eq $list_length
        and dim "None of the collections passed is currently listed"
        or win "Collections removed"

      case add

        # Verify argument validity
        if echo (math (count $argv) / 3) $argv[(seq 2 3 (count $argv))] \
        | string match -qvr '\d'
          err "$cmd: Invalid syntax"
          "$cmd"_instructions "$cmd -a/--\S+"
        end

        # Check collection availability
        for i in (command seq 1 3 (count $argv) | command sort -r)
          if not contains $argv[$i] \
          $collections[(command seq 1 3 (count $collections))]
            command curl --connect-timeout 60 -o /dev/null \
            -sIf https://unsplash.com/collections/$argv[$i]
            and continue
            err "$cmd: $argv[$i]: Collection was not found. Check its number or your connection."
          else
            dim "Collection |$argv[$i]| already present in the collection list"
          end
          set --erase argv[$i..(math $i + 2)]
        end
        test "$argv"
        or return 1

        # Add contents to collection list
        set -l parameters Collection Photocount Name
        set -l collections (tail +2 "$collections_list" 2>/dev/null \
        | string match -ar '\S+')
        for i in (command seq (count $parameters))
          echo $parameters[$i] > "$PREFIX"/tmp/$parameters[$i]
          printf '%s\n' $collections[(seq $i 3 (count $collections))] \
          $argv[(seq $i 3 (count $argv))] >> "$PREFIX"/tmp/$parameters[$i]
        end
        command pr -mt "$PREFIX"/tmp/{(string join , $parameters)} > "$collections_list"
        command rm "$PREFIX"/tmp/{(string join , $parameters)}

      case ''

        # Select a listed collection at random if none was passed
        if test -z "$argv"
          set argv (tail +2 "$collections_list" 2>/dev/null \
          | string match -ar '\S+')
          if test -z "$argv"
            err "$cmd: No collection described and no collection list available"
            return 1
          end
          set argv $argv[(command seq 2 3 (count $argv))]
          for i in (command seq 2 (count $argv))
            set argv[$i] (math $argv[$i] + $argv[(math $i - 1)])
          end
          set -l lottery (random 1 $argv[-1])
          for i in (command seq (count $argv))
            test $lottery -le $argv[$i]
            or continue
            set -l tmp (grep -oP '^\d+' "$collections_list")
            set argv $tmp[$i]
            break
          end
        end

        # Retrieve wallpaper and, if possible, set it as the current wallpaper
        mkdir -p $wallpapers_folder
        cd $wallpapers_folder
        type -qf unsplash-wallpaper
        and command unsplash-wallpaper -o $argv 1>&2
        or /usr/local/bin/unsplash-wallpaper -o $argv 1>&2
        set wallpapers (ls -t | string match -ar '^wallpaper-.+\.jpe?g$')
        if type -qf gsettings
          set -l PID (command pgrep gnome-session)
          set -x DBUS_SESSION_BUS_ADDRESS \
          (command grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ | command cut -d= -f2-)
          gsettings set org.gnome.desktop.background picture-uri \
          file://(command realpath $wallpapers[1])
          gsettings set org.gnome.desktop.screensaver picture-uri \
          file://(command realpath $wallpapers[1])
        else if type -qf termux-wallpaper
          termux-wallpaper -f $wallpapers[1]
          termux-wallpaper -lf $wallpapers[1]
        end

        # Delete old wallpapers according to cache size
        test (count $wallpapers) -gt $wallpaper_cache
        and rm $wallpapers[(math $wallpaper_cache + 1)..-1]
        prevd
    end
  end

  # Call main function and unload auxiliary functions
  "$cmd"_main $argv
  set -l exit_status $status
  functions -e (functions | string match -ar "^$cmd.+") dependency
  test $exit_status -eq 0
end
