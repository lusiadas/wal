function wal_instructions
  set -l bld (set_color 00afff -o)
  set -l reg (set_color normal)
  set -l instructions $bld"wal

"$bld"DECRIPTION

List unsplash photo collections from where to get a random wallpaper.

"$bld"OPTIONS

"$bld"wal"$reg" [collection number]
Get a random image from a collection and set it as the new background and screensaver wallpaper. If no collection is specified, a random listed collection will be used instead.

"$bld"wal"$reg" -a/--add [name] [collection number] [photocount] ...
Add a new collection to the list. Its name must not contain whitespaces.

"$bld"wal"$reg" -r/--remove [collection number] ...
Remove a collection from the list.

"$bld"wal"$reg" -u/--url [collection number] ...
Present the urls of specified collections.

"$bld"wal"$reg" -f/-folder [directory]
Set where images are stored. If no directory is passed, display where they are currently being stored.

"$bld"wal"$reg" -c/--cache [number]
Set the size of the image cache.

"$bld"wal"$reg" -l/--list [collection/size/description]
Show listed collections.

"$bld"wal"$reg" -h/--help
Display these instructions.
"
  string match -- '*/--\S+' "$argv"
  and echo $instructions | grep -A 1 -E "$argv" 1>&2
  or echo $instructions | less -R
end
